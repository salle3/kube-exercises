mas documentación en: https://gitlab.com/salle3/kube-exercises/-/issues/6

# Crear el replicaset:
He tenido varios errores importantes, con los labels y nombres, porfin resuelto
```
kubectl apply -f replicaSet.yaml
```
## asegurarse de tener instalado el plugin de minikube para usar el ingress
```
minikube addons enable ingress
```

## Levantar el servicio y assegurarse que se sincronizo con los pods del replicaset creados
```
kubectl apply -f service.yaml
kubectl describe -f service.yaml
```

## Crear el ingress y assegurarse con los pods
```
kubectl apply -f ingress.yaml
kubectl describe -f ingress.yaml
```

## Modificar `/etc/hosts`
modificar el fichero para ser rediccionado, para obtener la IP:
```
minikube service nginx-service --url
```

## Test
```
curl arnausistachreinoso.student.lasalle.com
```

# Crear un certificado
```
KEY_FILE=key.pem
CERT_FILE=cert.pem
HOST=arnausistachreinoso.student.lasalle.com
openssl req \
	-x509 \
	-nodes \
	-days 365 \
	-newkey rsa:2048 \
	-keyout ${KEY_FILE} \
	-out ${CERT_FILE} \
	-subj "/CN=${HOST}/O=${HOST}"
```

## Crear el secreto
```
KEY_FILE  = key.pem
CERT_FILE = cert.pem
CERT_NAME = nginx-cert
kubectl create secret tls ${CERT_NAME} --key ${KEY_FILE} --cert ${CERT_FILE}
```

## Modificar el ingress
Añadi el ingress.yaml, en spec:
```yaml
  tls:
    - hosts:
      - arnausistachreinoso.student.lasalle.com
      secretName: nginx-cert
```
esto esta en el fichero cert-ingress.yaml

Se aplica con:
```
kubectl apply -f cert-ingress.yaml
```
