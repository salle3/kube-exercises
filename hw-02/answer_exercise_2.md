# Tener 10 replicas
```
kubectl scale --replicas=10 -f ReplicaSet.yml
```

# Si necesito tener una replica en cada uno de los nodos de Kubernetes, ¿qué objeto se adaptaría mejor?
La mejor opción es [DaemonSet](https://kubernetes.io/docs/concepts/workloads/controllers/daemonset/).
