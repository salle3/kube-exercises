# Diseña una estrategia de despliegue que se base en ”Blue Green”.

## Existe una aplicación que está desplegada en el clúster
Ejecutar: `make blue`

- Creamos un deploy con nombre y versión
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-1.10
```
En este caso lo llamo `blue.yaml`.

- Lo relacionamos con el exterior para poder atender a los clientes:
Ejecutar: `make service`

## Antes de ofrecer el servicio a los usuarios, la compañía necesita realizar una serie de validaciones con la versión 2.0
Ejecutar: `make green`

- Creamos el deploy con la nueva versión, y en local lo provamos
- La diferencia entre ficheros no tiene que ser mayor que:
  - Nombre, para tener deploys distintos
  - Labels, para que el service no se confunda
  - Imagen, para sufrir el update! (imagen o lo que se quiera hacer update)
```diff
--- blue.yaml	2020-11-18 18:31:58.067028579 +0100
+++ green.yaml	2020-11-18 18:31:59.723695296 +0100
@@ -1,7 +1,7 @@
 apiVersion: apps/v1
 kind: Deployment
 metadata:
-  name: nginx-1.10
+  name: nginx-1.11
   labels:
     app: nginx
 spec:
@@ -13,10 +13,10 @@
     metadata:
       labels:
         app: nginx
-        version: "1.10"
+        version: "1.11"
     spec:
       containers:
       - name: nginx
-        image: nginx:1.10
+        image: nginx:1.11
         ports:
         - containerPort: 80
```

## Una vez que el equipo ha validado la aplicación, se realiza un switch del tráfico a la versión 2.0 sin impacto para los usuarios
Ejecutar: `make serviceGrenn`

Se canvia el servició para que apunte al nuevo deploy.
Una vez se considere que es estable, se puede eliminar el anterior deploy o pausar en espera para poder hacer rollback.
