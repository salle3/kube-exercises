# Despliega una nueva versión de tu nuevo servicio mediante la técnica “recreate”
Añadí esto en el fichero yml.
```
spec:
  replicas: 3
  strategy:
    type: Recreate
```

# Despliega una nueva versión haciendo “rollout deployment”
```
kubectl set image deployment/nginx-deployment nginx=nginx:1.16.1 --record
```

# Realiza un rollback a la versión generada previamente
```
kubectl rollout undo -f ngingx-deployment.yml
```
