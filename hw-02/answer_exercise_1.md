# Todo lo que se necesita!
Gran parte documentada en: https://gitlab.com/salle3/kube-exercises/-/issues/1

# 10 Líneas de logs
Devolverá como máximo 10 líneas
```
kubectl logs hw-02 --tail 10
```

# IP interna del pod
```
kubectl get -f pod.yml --template={{.status.podIP}}
```

# Entrar al container:
```
kubectl exec --stdin --tty -f pod.yml -- /bin/sh
```

# Visualizar el contenido:
```
kubectl exec --tty -f pod.yml -- cat /usr/share/nginx/html/index.html
```
